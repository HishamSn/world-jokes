package com.jokesworld.zero.jokesworld.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jokesworld.zero.jokesworld.R;
import com.jokesworld.zero.jokesworld.ui.base.BaseAdapter;

import java.util.List;

public class MainAdapter extends BaseAdapter<MainAdapter.ViewHolder> {
    private static final int ROW_MEAL_ADDITIONS = R.layout.row_post;
    private List<String> choice;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemViewType(int position) {
        return ROW_MEAL_ADDITIONS;
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView tvAddition;

        public ViewHolder(View itemView) {
            super(itemView);
//            tvAddition = itemView.findViewById(R.id.tv_addition);
        }
    }
}
