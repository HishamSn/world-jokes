package com.jokesworld.zero.jokesworld.ui.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.jokesworld.zero.jokesworld.R;
import com.jokesworld.zero.jokesworld.ui.auth.LoginActivity;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {

    View viewHeaderNav;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggleBar;
    private NavigationView navigationView;
    private Button btnLogin;
    private Button btnSetting;
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        changeLanguage();
    }


    public void setNavigation(NavigationView navigationView, Toolbar toolbar,
                              DrawerLayout drawerLayout) {
        init(navigationView, toolbar, drawerLayout);

        toggleBar = new ActionBarDrawerToggle(this, drawerLayout, toolbar
                , R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggleBar);
        toggleBar.syncState();
        navigationView.getMenu().getItem(0).setChecked(true);
        setNavigationItemSelectedListener(navigationView);


        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        addHeader(navigationView);

        setUpViewHeaderNav();

    }

    private void init(NavigationView navigationView, Toolbar toolbar, DrawerLayout drawerLayout) {
        this.navigationView = navigationView;
        this.drawerLayout = drawerLayout;
        this.toolbar = toolbar;

    }
    private void checkIsUserLogin() {
//        if (!SessionUtils.getInstance().isLogin()) {
//            btnLogin.setText(getString(R.string.login_account));
//            btnSetting.setVisibility(View.GONE);
//        } else {
//            btnLogin.setText(getString(R.string.logout_account));
//            btnSetting.setVisibility(View.VISIBLE);
//        }
    }

    private void setNavigationItemSelectedListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(item -> {
            selectItemDrawer(item);
            return true;
        });
    }

    private void addHeader(NavigationView navigationView) {
        viewHeaderNav = navigationView.inflateHeaderView(R.layout.header_activity_main_navigation_view);
    }

    public void setUpViewHeaderNav() {
//        btnLogin = viewHeaderNav.findViewById(R.id.btn_login);
//        btnSetting = viewHeaderNav.findViewById(R.id.btn_setting);
        checkIsUserLogin();

//        btnLogin.setOnClickListener(v -> {
//
//            drawerLayout.closeDrawers();
//        });

//        btnSetting.setOnClickListener(v -> {
////            if (SessionUtils.getInstance().getUser() == null) {
////                Toasty.error(this, getString(R.string.issue_happened_clean_cache),
////                        Toast.LENGTH_SHORT, true).show();
////                return;
//
//
////            startActivity(new Intent(context, CustomerProfileActivity.class));
//            drawerLayout.closeDrawers();
//        });
//        viewHeaderNav.findViewById(R.id.btn_change_lang)
//                .setOnClickListener(v -> {
////                    Intent intent = new Intent(context, ChooseLanguageActivity.class);
////                    intent.putExtra("transition_activity", ActivityUtil.MAIN_ACTIVITY);
////                    startActivity(intent);
//                    drawerLayout.closeDrawers();
////                    finish();
//
//                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getItemId() == R.id.action_notfication) {
//            askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION, LOCATION);
//            return true;
//        }

        if (toggleBar.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void selectItemDrawer(MenuItem menuItem) {
        Intent intent;
        switch (menuItem.getItemId()) {
            case R.id.iMenuMainDrawerNavigationProfile:

                startActivity(new Intent(this,LoginActivity.class));

                break;
            case R.id.iMenuMainDrawerNavigationMyPosts:


                break;



        }
        drawerLayout.closeDrawers();


    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }


}
