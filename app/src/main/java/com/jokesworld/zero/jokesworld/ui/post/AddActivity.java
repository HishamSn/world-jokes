package com.jokesworld.zero.jokesworld.ui.post;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jokesworld.zero.jokesworld.R;
import com.jokesworld.zero.jokesworld.ui.base.BaseActivity;

public class AddActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
    }
}
