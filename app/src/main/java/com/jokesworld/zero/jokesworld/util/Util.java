package com.jokesworld.zero.jokesworld.util;

import android.app.ProgressDialog;
import android.content.Context;

public class Util {


    public static ProgressDialog progressUtil(Context context) {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        return progress;
//        progress.show();
// To dismiss the dialog
//        progress.dismiss();
    }
}
