package com.jokesworld.zero.jokesworld.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.jokesworld.zero.jokesworld.R;
import com.jokesworld.zero.jokesworld.ui.base.BaseActivity;
import com.jokesworld.zero.jokesworld.ui.post.AddActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.rv_post)
    RecyclerView rvPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpAppBar();

        rvPost.setAdapter(new MainAdapter());


    }

    void setUpAppBar() {
        setNavigation(
                findViewById(R.id.nv_main),
                findViewById(R.id.toolbar),
                findViewById(R.id.cl_parent)
        );

    }

    @OnClick(R.id.et_add_post)
    public void onViewClicked() {
        startActivity(new Intent(this,AddActivity.class));
    }
}
