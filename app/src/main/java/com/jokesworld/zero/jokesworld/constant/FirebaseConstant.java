package com.jokesworld.zero.jokesworld.constant;

public final class FirebaseConstant {

    public static final String COMPANY_TABLE = "Company";
    public static final String MY_CAR_TABLE = "myCars";
    public static final String USERS_TABLE = "Users";
    public static final String BUSY_DATE_TABLE = "busyDate";
    public static final String RESERVATION_TABLE = "reservation";
    public static final String RESERVE_HELP_TABLE = "reserveHelp";
    public static final String ADMIN_ACCOUNT = "admin@admin.com";
}
