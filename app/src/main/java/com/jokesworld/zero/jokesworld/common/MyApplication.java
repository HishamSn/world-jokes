package com.jokesworld.zero.jokesworld.common;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class MyApplication extends Application {
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseApp.initializeApp(this);

    }

    public static synchronized MyApplication getInstance() {
        return instance;
    }

}
