package com.jokesworld.zero.jokesworld.data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.jokesworld.zero.jokesworld.common.MyApplication;

public class PrefsUtils {


    private static final String ADMIN = "admin";

    private static PrefsUtils instance;
    private SharedPreferences prefs;

    private PrefsUtils() {
        prefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance());
    }

    public static synchronized PrefsUtils getInstance() {
        if (instance == null) {
            instance = new PrefsUtils();
        }
        return instance;
    }

    public boolean isAdmin() {
        return prefs.getBoolean(ADMIN, false);
    }

    public void setAdmin(boolean enable) {
        prefs.edit().putBoolean(ADMIN, enable).apply();
    }

}
