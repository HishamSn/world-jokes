package com.jokesworld.zero.jokesworld.model.user;

import java.util.List;

public class User {
    private String id;

    private String name;
    private String phone;
    private String email;

//    private List<MyCars> myCars;

    public String getName() {
        return name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
